package com.demkom58.liteedge.engine.database

interface DatabasePreparer {

    fun fullPrepare()

}