package com.demkom58.liteedge.engine.database

import com.demkom58.liteedge.engine.core.configurations.DatabaseConfiguration
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException

class LiteDatabaseRequester(private val configuration: DatabaseConfiguration) : DatabaseRequester {
    override val preparer: DatabasePreparer = LiteDatabasePreparer(this)

    override val connection: Connection
        @Throws(SQLException::class)
        get() = DriverManager.getConnection(configuration.connectString + configuration.database + configuration.args, configuration.login, configuration.password)

    init {
        try {
            Class.forName(configuration.className)
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
    }
}
