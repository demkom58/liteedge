package com.demkom58.liteedge.engine.database

import java.sql.Connection

interface DatabaseRequester {

    val preparer: DatabasePreparer
    val connection: Connection

}