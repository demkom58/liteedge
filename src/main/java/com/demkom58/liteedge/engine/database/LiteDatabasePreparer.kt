package com.demkom58.liteedge.engine.database

import java.sql.Connection
import java.sql.SQLException

class LiteDatabasePreparer(private val requester: LiteDatabaseRequester) : DatabasePreparer {

    override fun fullPrepare() {
        makeUsersTable()
        makeProfileCommentsTable()
        makeProfileCommentRatingsTable()
    }

    fun makeUsersTable() {
        request(
                "CREATE TABLE IF NOT EXISTS `le_users`( " +
                        "`id` INT(10) NOT NULL AUTO_INCREMENT, " +
                        "`user_id` VARCHAR(10) NULL, " +

                        "`session_hash` VARCHAR(32) NOT NULL," +
                        "`password_hash` VARCHAR(32) NOT NULL, " +

                        "`last_ip` VARCHAR(39) NOT NULL, " +
                        "`user_agent` VARCHAR(700) NOT NULL, " +
                        "`time_zone` VARCHAR(30) NOT NULL, " +
                        "`register_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, " +

                        "`avatar` VARCHAR(255) NULL, " +
                        "`email` VARCHAR(30) NOT NULL, " +
                        "`nick` VARCHAR(15) NOT NULL, " +
                        "`phone` VARCHAR(15) NULL, " +
                        "`realname` VARCHAR(35) NULL, " +
                        "`about` VARCHAR(6000) NULL, " +
                        "`rating` INT(6) NOT NULL DEFAULT '0', " +

                        "`banned` TIMESTAMP NULL, " +
                        "`user_group` INT(5) NOT NULL DEFAULT '1', " +

                        "`news_posted_num` INT(6) NOT NULL DEFAULT '0', " +
                        "`threads_posted_num` INT(6) NOT NULL DEFAULT '0', " +
                        "`news_commented_num` INT(6) NOT NULL DEFAULT '0', " +
                        "`threads_commented_num` INT(6) NOT NULL DEFAULT '0', " +

                        "`profile_comments` TEXT DEFAULT NULL, " +

                        "PRIMARY KEY (`id`), " +
                        "UNIQUE (`user_id`));"
        )
    }

    fun makeProfileCommentsTable() {
        request("CREATE TABLE IF NOT EXISTS `le_profile_comments`( " +
                "`id` INT(10) NOT NULL AUTO_INCREMENT, " +
                "`user_id` INT(3) NOT NULL, " +
                "`content` VARCHAR(10000) NOT NULL, " +
                "PRIMARY KEY (`id`));"
        )
    }

    fun makeProfileCommentRatingsTable() {
        request("CREATE TABLE IF NOT EXISTS `le`.`le_profile_comments_rating`( " +
                "`id` INT(10) NOT NULL AUTO_INCREMENT, " +
                "`author_id` INT(10) NOT NULL, " +
                "`comment_id` INT(10) NOT NULL, " +
                "`rating_type` INT(3) NOT NULL, " +
                "PRIMARY KEY (`id`), " +
                "UNIQUE (`author_id`));"
        )
    }

    private fun request(sql: String) {
        val connection: Connection
        try {
            connection = requester.connection
            val statement = connection.createStatement()

            statement.execute(sql)

            statement.close()
            connection.close()
        } catch (e: SQLException) {
            e.printStackTrace()
        }
    }

}
