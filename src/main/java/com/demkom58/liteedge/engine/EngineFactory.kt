package com.demkom58.liteedge.engine

import com.demkom58.liteedge.LiteEdge
import com.demkom58.liteedge.engine.core.managers.page.PageManager
import com.demkom58.liteedge.util.StringUtil
import io.undertow.Handlers
import io.undertow.Undertow
import io.undertow.server.handlers.resource.FileResourceManager
import io.undertow.server.handlers.resource.ResourceHandler
import java.io.File


class EngineFactory private constructor() {
    fun build(): Undertow {
        val pm: PageManager = LiteEdge.instance.liteCore.pageManager
        val builder = Undertow.builder()!!
        builder.addHttpListener(666, "localhost")

        val handler = Handlers.path()
        for (page in pm.getExactPages()) handler.addExactPath(page.path, page.handler)
        for (page in pm.getPrefixPages()) handler.addPrefixPath(page.path, page.handler)

        handler.addPrefixPath("r", ResourceHandler(FileResourceManager(File(StringUtil.getResourceDirPath()))))
        builder.setHandler(handler)

        return builder.build()
    }

    companion object {
        private var builder: EngineFactory? = null

        @Synchronized
        fun getBuilder(): EngineFactory {
            if (builder == null) builder = EngineFactory()
            return builder as EngineFactory
        }
    }
}
