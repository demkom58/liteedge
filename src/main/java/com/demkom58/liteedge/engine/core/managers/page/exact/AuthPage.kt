package com.demkom58.liteedge.engine.core.managers.page.exact

import com.demkom58.liteedge.engine.core.managers.page.root.Page
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.engine.exceptions.DatabaseException
import com.demkom58.liteedge.engine.exceptions.InvalidUserDataException
import com.demkom58.liteedge.engine.exceptions.UserException
import com.demkom58.liteedge.util.UserExchangeAuthUtil
import freemarker.template.Template
import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import io.undertow.util.StatusCodes

class AuthPage(path: String, templateName: String) : Page(path, templateName) {

    override fun handler(): HttpHandler {
        return makeHandler()
    }

    override fun handle(exchange: HttpServerExchange, template: Template) {
        val action = getGetParam("action", exchange)

        when (action) {
            "login" -> login(exchange, getGetParam("login", exchange), getGetParam("password", exchange))
            "logout" -> logout(exchange)
            else -> sendSafePage(exchange, template)
        }
    }

    private fun login(exchange: HttpServerExchange, login: String?, password: String?) {
        try {
            val user: User = try {
                UserExchangeAuthUtil.createUserSession(exchange, login, password)
            } catch (e: UserException) {
                exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
                exchange.queryParameters.clear()
                exchange.responseHeaders.put(Headers.LOCATION, "/login")
                exchange.endExchange()
                return
            }
            exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
            exchange.queryParameters.clear()
            exchange.responseHeaders.put(Headers.LOCATION, "/profile/" + user.id)
            exchange.endExchange()
        } catch (e: InvalidUserDataException) {
            exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
            exchange.queryParameters.clear()
            exchange.responseHeaders.put(Headers.LOCATION, "/login")
            exchange.endExchange()
        } catch (e: DatabaseException) {
            e.printStackTrace()
            error(exchange, e.message)
        }
    }

    private fun logout(exchange: HttpServerExchange) {
        UserExchangeAuthUtil.logoutSession(exchange)

        exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
        exchange.queryParameters.clear()
        exchange.responseHeaders.put(Headers.LOCATION, "/login")
        exchange.endExchange()
    }

    override fun handle(exchange: HttpServerExchange, template: Template, user: User) {
        val action: String? = getGetParam("action", exchange)

        if (action == "logout") {
            logout(exchange)
            return
        }

        exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
        exchange.queryParameters.clear()
        exchange.responseHeaders.put(Headers.LOCATION, "/profile/" + user.id)
        exchange.endExchange()

    }

}
