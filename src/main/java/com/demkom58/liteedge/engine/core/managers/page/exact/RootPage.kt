package com.demkom58.liteedge.engine.core.managers.page.exact

import com.demkom58.liteedge.engine.core.managers.page.root.Page
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import freemarker.template.Template
import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange

class RootPage(path: String, templateName: String) : Page(path, templateName) {

    override fun handler(): HttpHandler {
        return makeHandler()
    }

    override fun handle(exchange: HttpServerExchange, template: Template) {
        sendSafePage(exchange, template)
    }

    override fun handle(exchange: HttpServerExchange, template: Template, user: User) {
        handle(exchange, template)
    }

}
