package com.demkom58.liteedge.engine.core.managers.configuration

interface ConfigurationManager {

    fun saveConfiguration(name: String, target: Any)
    fun saveDefaultConfiguration(name: String, target: Any)
    fun <T> updateConfiguration(name: String, type: Class<T>): T?
    fun registerConfiguration(name: String, config: Any)
    fun registerConfigurationAndSaveDefault(name: String, config: Any)
    fun unregisterConfiguration(name: String)
    fun <T> getConfiguration(name: String, type: Class<T>): T

}