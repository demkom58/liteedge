package com.demkom58.liteedge.engine.core.managers.page.root

import io.undertow.server.HttpHandler

interface IPage {

    var path: String
    var templateName: String
    var handler: HttpHandler

}
