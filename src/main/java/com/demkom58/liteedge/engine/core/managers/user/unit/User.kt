package com.demkom58.liteedge.engine.core.managers.user.unit

class User(
        val id: Int,
        var userID: String?,
        var sessionHash: String,
        var passwordHash: String,
        var lastIP: String,
        var userAgent: String,
        var timeZone: String,
        var registerDate: String,
        var avatar: String?,
        var email: String,
        var nick: String,
        var phone: String?,
        var realname: String?,
        var about: String?,
        var rating: Int,
        var banned: String?,
        var userGroup: Int,
        var newsPosted: Int,
        var threadsPosted: Int,
        var newsCommented: Int,
        var threadsCommented: Int,
        iProfileComments: String?

) {
    var parsedProfileComments: List<Int>? = ArrayList()
        private set

    var profileComments: String? = iProfileComments
        set(value) {
            field = value
            updateInternalProfileComments()
        }

    fun applyNewComment(id: Int) {
        profileComments += "$id:"
        updateInternalProfileComments()
    }

    init {
        updateInternalProfileComments()
    }

    private fun updateInternalProfileComments() {
        if (profileComments == null) return

        val spar: List<String>? = profileComments?.split(":")?.toList()
        parsedProfileComments = ArrayList(spar!!.size)
        for (i in spar) {
            if (i.isEmpty()) continue
            parsedProfileComments?.plus(i.toInt())
        }
    }

}




