package com.demkom58.liteedge.engine.core.unit

enum class ImageAccepted(val mime: String) {
    jpeg("jpg"),
    png("png"),
    gif("gif"),
    bmp("bmp")
}