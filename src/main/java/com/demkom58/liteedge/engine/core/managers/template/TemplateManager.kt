package com.demkom58.liteedge.engine.core.managers.template

import freemarker.template.Template

interface TemplateManager {

    fun registerTemplate(name: String, template: Template)
    fun registerTemplate(name: String, template: String)
    fun unregisterTemplate(name: String)
    fun getTemplate(name: String): Template?

}