package com.demkom58.liteedge.engine.core.managers.user

import com.demkom58.liteedge.engine.core.managers.user.unit.User

interface UserManager {

    fun getUser(login: String): User?
    fun getUser(id: Int): User?
    fun createUser(email: String, nick: String, sessionHash: String, passwordHash: String, lastIP: String, userAgent: String, timeZone: String): Boolean
    fun createUser(user: User): Boolean
    fun updateUser(user: User): Boolean
    fun existUserByLogin(login: String): Boolean
    fun existUserByNick(nick: String): Boolean
    fun existUserByEMail(email: String): Boolean
    fun existUserByID(id: Int): Boolean

    fun isValidEmail(email: String?): Boolean
    fun isValidNickname(nick: String?): Boolean
    fun isValidPassword(password: String?): Boolean

    fun getSessionHash(email: String, nick: String, passHash: String, timeZone: String, userAgent: String): String

}