package com.demkom58.liteedge.engine.core.managers.profile

import com.demkom58.liteedge.engine.core.Core
import com.demkom58.liteedge.engine.core.managers.resource.ResourceManager
import com.demkom58.liteedge.engine.core.managers.user.UserManager
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.engine.core.unit.ImageAccepted
import java.io.File
import javax.activation.MimetypesFileTypeMap

class LiteProfileManager(val core: Core, val resourceManager: ResourceManager, val userManager: UserManager) : ProfileManager {

    @Throws(UnsupportedOperationException::class)
    override fun updateAvatar(image: File, user: User) {

        val type = try {
            ImageAccepted.valueOf(MimetypesFileTypeMap().getContentType(image).split("/")[1]).toString()
        } catch (e: IllegalArgumentException) {
            throw UnsupportedOperationException("Unsupported image extension!")
        }

        image.renameTo(resourceManager.getAvatarFile("${user.id}.$type"))
        user.avatar = type
        userManager.updateUser(user)

    }

}