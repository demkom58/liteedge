package com.demkom58.liteedge.engine.core.managers.page

import com.demkom58.liteedge.engine.core.managers.page.root.IPage

interface PageManager {

    fun clearExactPages()
    fun registerExactPage(page: IPage)
    fun unregisterExactPage(page: IPage)
    fun getExactPages(): List<IPage>
    fun clearPrefixPages()
    fun registerPrefixPage(page: IPage)
    fun unregisterPrefixPage(page: IPage)
    fun getPrefixPages(): List<IPage>

}