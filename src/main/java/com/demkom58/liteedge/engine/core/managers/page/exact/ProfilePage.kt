package com.demkom58.liteedge.engine.core.managers.page.exact

import com.demkom58.liteedge.engine.core.managers.page.root.Page
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.util.StringUtil
import freemarker.template.Template
import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import io.undertow.util.StatusCodes
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class ProfilePage(path: String, templateName: String) : Page(path, templateName) {

    override fun handler(): HttpHandler {
        return makeAdvancedHandler()
    }

    override fun handle(exchange: HttpServerExchange, template: Template, user: User) {
        val pathParams = getPathParam(exchange)
        val action = getPostParam("action", exchange)

        if (action == "avatar") {
            loadAvatar(exchange, template, user)
            return
        }

        val id: String = when (pathParams) {
            null -> {
                processLocalPage(exchange, template, user)
                return
            }
            else -> pathParams[0]
        }

        if (user.id.toString() == id || user.userID == id) processLocalPage(exchange, template, user) else processExactPage(exchange, template, user, id)
    }

    override fun handle(exchange: HttpServerExchange, template: Template) {
        exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
        exchange.queryParameters.clear()
        exchange.responseHeaders.put(Headers.LOCATION, "/login")
        exchange.endExchange()
    }

    private fun processExactPage(exchange: HttpServerExchange, template: Template, visitor: User, id: String) {
        val owner: User? = try {
            core.userManager.getUser(id.toInt())
        } catch (e: Exception) {
            processLocalPage(exchange, template, visitor)
            return
        }

        if (owner == null) {
            error(exchange, "User not found!")
            return
        }

        val placeholders = HashMap<String, Any>()
        placeholders["title"] = "${owner.nick}'s profile"
        placeholders["visitor_nick"] = visitor.nick
        placeholders["reg_date"] = owner.registerDate
        placeholders["email"] = owner.email
        placeholders["news_posted"] = owner.newsPosted
        placeholders["threads_posted"] = owner.threadsPosted
        placeholders["city"] = StringUtil.getCity(exchange.sourceAddress.address.hostAddress)
        placeholders["avatar"] = core.resourceManager.getAvatarLink(owner)

        val dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        val now = LocalDateTime.now()
        placeholders["now"] = dtf.format(now)

        if (owner.phone == null) placeholders["phone"] = "unknown"
        else placeholders["phone"] = owner.phone!!

        if (owner.about == null) placeholders["about"] = "anonim"
        else placeholders["about"] = owner.about!!

        sendSafePage(exchange, placeholders, template)
    }

    private fun processLocalPage(exchange: HttpServerExchange, template: Template, user: User) {
        val placeholders = HashMap<String, Any>()
        placeholders["title"] = "Your Profile"
        placeholders["visitor_nick"] = user.nick
        placeholders["reg_date"] = user.registerDate
        placeholders["email"] = user.email
        placeholders["city"] = StringUtil.getCity(exchange.sourceAddress.address.hostAddress)
        placeholders["news_posted"] = user.newsPosted
        placeholders["threads_posted"] = user.threadsPosted
        placeholders["avatar"] = core.resourceManager.getAvatarLink(user)

        val dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
        val now = LocalDateTime.now()
        placeholders["now"] = dtf.format(now)

        if (user.phone == null) placeholders["phone"] = "unknown"
        else placeholders["phone"] = user.phone!!

        if (user.about == null) placeholders["about"] = "It's a void..."
        else placeholders["about"] = user.about!!

        sendSafePage(exchange, placeholders, template)
    }

    private fun loadAvatar(exchange: HttpServerExchange, template: Template, user: User) {

    }
}