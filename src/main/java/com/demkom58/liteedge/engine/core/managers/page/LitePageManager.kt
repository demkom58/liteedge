package com.demkom58.liteedge.engine.core.managers.page

import com.demkom58.liteedge.engine.core.Core
import com.demkom58.liteedge.engine.core.managers.page.root.IPage
import java.util.*

class LitePageManager(core: Core) : PageManager {
    private val exactPages = ArrayList<IPage>()
    private val prefixPages = ArrayList<IPage>()

    override fun clearExactPages() {
        exactPages.clear()
    }

    override fun registerExactPage(page: IPage) {
        exactPages.add(page)
    }

    override fun unregisterExactPage(page: IPage) {
        exactPages.remove(page)
    }

    override fun getExactPages(): List<IPage> {
        return exactPages
    }

    override fun clearPrefixPages() {
        prefixPages.clear()
    }

    override fun registerPrefixPage(page: IPage) {
        prefixPages.add(page)
    }

    override fun unregisterPrefixPage(page: IPage) {
        prefixPages.remove(page)
    }

    override fun getPrefixPages(): List<IPage> {
        return prefixPages
    }

}