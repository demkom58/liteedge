package com.demkom58.liteedge.engine.core

import com.demkom58.liteedge.engine.core.managers.configuration.ConfigurationManager
import com.demkom58.liteedge.engine.core.managers.page.PageManager
import com.demkom58.liteedge.engine.core.managers.profile.ProfileManager
import com.demkom58.liteedge.engine.core.managers.resource.ResourceManager
import com.demkom58.liteedge.engine.core.managers.template.TemplateManager
import com.demkom58.liteedge.engine.core.managers.user.UserManager
import com.demkom58.liteedge.engine.database.DatabaseRequester
import com.maxmind.geoip2.DatabaseReader

interface Core {

    val requester: DatabaseRequester
    val geoIpReader: DatabaseReader

    val configurationManager: ConfigurationManager
    val resourceManager: ResourceManager
    val templateManager: TemplateManager
    val pageManager: PageManager
    val userManager: UserManager
    val profileManager: ProfileManager

}