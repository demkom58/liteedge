package com.demkom58.liteedge.engine.core.managers.configuration

import com.demkom58.liteedge.engine.core.Core
import com.demkom58.liteedge.util.StringUtil
import com.google.gson.GsonBuilder
import java.io.File
import java.io.FileReader
import java.io.FileWriter


class LiteConfigurationManager(val core: Core) : ConfigurationManager {
    private val gSon = GsonBuilder().setPrettyPrinting().setLenient().disableHtmlEscaping().create()
    private val configurations = HashMap<String, Any>()

    init {
        val configurations = File(StringUtil.getConfigurationsDirPath())
        configurations.mkdirs()
    }

    override fun saveConfiguration(name: String, target: Any) {
        val writer = FileWriter(StringUtil.getConfigurationsDirPath() + name)
        gSon.toJson(target, writer)
        writer.flush()
        writer.close()
    }

    override fun saveDefaultConfiguration(name: String, target: Any) {
        val file = File(StringUtil.getConfigurationsDirPath() + name)
        if (!file.exists()) saveConfiguration(name, target)
    }

    override fun <T> updateConfiguration(name: String, type: Class<T>): T? {
        val reader = FileReader(StringUtil.getConfigurationsDirPath() + name)
        val result = gSon.fromJson(reader.readText(), type)
        reader.close()

        registerConfiguration(name, result!!)
        return result
    }

    override fun registerConfiguration(name: String, config: Any) {
        configurations[name] = config
    }

    override fun registerConfigurationAndSaveDefault(name: String, config: Any) {
        configurations[name] = config
        saveDefaultConfiguration(name, config)
    }

    override fun unregisterConfiguration(name: String) {
        configurations.remove(name)
    }

    override fun <T> getConfiguration(name: String, type: Class<T>): T {
        return configurations[name]!! as T
    }


}