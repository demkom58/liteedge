package com.demkom58.liteedge.engine.core.managers.template

import com.demkom58.liteedge.LiteEdge
import com.demkom58.liteedge.engine.core.Core
import freemarker.template.Configuration
import freemarker.template.Template
import freemarker.template.TemplateExceptionHandler
import java.util.*

class LiteTemplateManager(core: Core) : TemplateManager {
    private var configuration: Configuration = Configuration(Configuration.VERSION_2_3_28)
    private var templates = HashMap<String, Template>()

    init {
        configuration.setClassLoaderForTemplateLoading(LiteEdge::class.java.classLoader, "templates")
        configuration.defaultEncoding = "UTF-8"
        configuration.templateExceptionHandler = TemplateExceptionHandler.HTML_DEBUG_HANDLER
    }

    override fun registerTemplate(name: String, template: Template) {
        templates[name] = template
    }

    override fun registerTemplate(name: String, template: String) {
        templates[name] = configuration.getTemplate(template)
    }

    override fun unregisterTemplate(name: String) {
        templates.remove(name)
    }

    override fun getTemplate(name: String): Template? {
        return templates[name]
    }
}