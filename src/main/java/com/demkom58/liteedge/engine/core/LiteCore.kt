package com.demkom58.liteedge.engine.core

import com.demkom58.liteedge.engine.core.configurations.DatabaseConfiguration
import com.demkom58.liteedge.engine.core.managers.configuration.ConfigurationManager
import com.demkom58.liteedge.engine.core.managers.configuration.LiteConfigurationManager
import com.demkom58.liteedge.engine.core.managers.page.LitePageManager
import com.demkom58.liteedge.engine.core.managers.page.PageManager
import com.demkom58.liteedge.engine.core.managers.profile.LiteProfileManager
import com.demkom58.liteedge.engine.core.managers.profile.ProfileManager
import com.demkom58.liteedge.engine.core.managers.resource.LiteResourceManager
import com.demkom58.liteedge.engine.core.managers.resource.ResourceManager
import com.demkom58.liteedge.engine.core.managers.template.LiteTemplateManager
import com.demkom58.liteedge.engine.core.managers.template.TemplateManager
import com.demkom58.liteedge.engine.core.managers.user.LiteUserManager
import com.demkom58.liteedge.engine.core.managers.user.UserManager
import com.demkom58.liteedge.engine.database.DatabaseRequester
import com.demkom58.liteedge.engine.database.LiteDatabaseRequester
import com.maxmind.db.CHMCache
import com.maxmind.geoip2.DatabaseReader
import java.io.IOException

class LiteCore : Core {
    override lateinit var requester: DatabaseRequester
    override lateinit var geoIpReader: DatabaseReader

    override lateinit var configurationManager: ConfigurationManager
    override lateinit var resourceManager: ResourceManager
    override lateinit var templateManager: TemplateManager
    override lateinit var pageManager: PageManager
    override lateinit var userManager: UserManager
    override lateinit var profileManager: ProfileManager

    init {
        initConfigurations()

        requester = LiteDatabaseRequester(configurationManager.getConfiguration("database.json", DatabaseConfiguration::class.java))
        requester.preparer.fullPrepare()

        resourceManager = LiteResourceManager(this)
        templateManager = LiteTemplateManager(this)
        pageManager = LitePageManager(this)
        userManager = LiteUserManager(this, requester)
        profileManager = LiteProfileManager(this, resourceManager, userManager)

        initGeoIP()
    }

    @Throws(NullPointerException::class)
    private fun initGeoIP() {
        try {
            geoIpReader = DatabaseReader.Builder(resourceManager.downloadGeoDatabase()).withCache(CHMCache()).build()
        } catch (e: IOException) {
            e.printStackTrace()
            throw NullPointerException("Not GeoLite Database found.")
        }
    }

    private fun initConfigurations() {
        configurationManager = LiteConfigurationManager(this)

        val dConf = DatabaseConfiguration(
                "com.mysql.cj.jdbc.Driver",
                "jdbc:mysql://localhost:3306/",
                "le",
                "?verifyServerCertificate=false" + "&useSSL=false" + "&requireSSL=false" + "&useLegacyDatetimeCode=false" + "&amp" + "&serverTimezone=UTC",
                "demkom",
                "demkom"
        )
        configurationManager.registerConfigurationAndSaveDefault("database.json", dConf)
        configurationManager.updateConfiguration("database.json", dConf.javaClass)
    }

}