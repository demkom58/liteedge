package com.demkom58.liteedge.engine.core.managers.page.root

import com.demkom58.liteedge.LiteEdge
import com.demkom58.liteedge.engine.core.Core
import com.demkom58.liteedge.engine.core.Errors
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.engine.exceptions.NoSuchUserException
import com.demkom58.liteedge.util.UserExchangeAuthUtil
import freemarker.template.Template
import freemarker.template.TemplateException
import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.server.handlers.form.EagerFormParsingHandler
import io.undertow.server.handlers.form.FormDataParser
import io.undertow.server.handlers.form.FormParserFactory
import io.undertow.server.handlers.form.MultiPartParserDefinition
import io.undertow.util.Headers
import java.io.IOException
import java.io.StringWriter
import java.sql.Connection
import java.sql.SQLException
import java.util.*


abstract class Page(override var path: String, override var templateName: String) : IPage {
    protected val core: Core = LiteEdge.instance.liteCore

    final override var handler: HttpHandler

    protected val databaseConnection: Connection
        @Throws(SQLException::class)
        get() = core.requester.connection

    init {
        handler = handler()
    }

    protected fun makeHandler(): HttpHandler {
        return HttpHandler { exchange ->
            exchange.responseHeaders.put(Headers.ACCEPT_ENCODING, "UTF-8")
            exchange.responseHeaders.put(Headers.CACHE_CONTROL, "no-cache, no-store, must-revalidate")
            exchange.responseHeaders.put(Headers.PRAGMA, "no-cache")
            exchange.responseHeaders.put(Headers.EXPIRES, 0)

            val template: Template? = getTemplate(templateName)
            if (template == null) {
                error(exchange, Errors.NO_TEMPLATE_FOUND)
                return@HttpHandler
            }

            val userEx: User =
                    try {
                        UserExchangeAuthUtil.getUserBySession(exchange)
                    } catch (e: NoSuchUserException) {
                        handle(exchange, template)
                        return@HttpHandler
                    }
            handle(exchange, template, userEx)
        }
    }

    protected fun makeAdvancedHandler(): HttpHandler {
        val builder = FormParserFactory.builder()
        builder.addParsers(MultiPartParserDefinition())
        builder.defaultCharset = "UTF-8"
        return EagerFormParsingHandler(builder.build()).setNext(makeHandler())
    }

    protected abstract fun handler(): HttpHandler
    protected abstract fun handle(exchange: HttpServerExchange, template: Template)
    protected abstract fun handle(exchange: HttpServerExchange, template: Template, user: User)

    protected fun getTemplate(name: String): Template? {
        return core.templateManager.getTemplate(name)
    }

    protected fun sendSafePage(exchange: HttpServerExchange, placeholders: Map<String, Any>, template: Template) {
        exchange.responseHeaders.put(Headers.CONTENT_TYPE, "text/html")
        val writer = StringWriter()
        try {
            template.process(placeholders, writer)
        } catch (e: TemplateException) {
            error(exchange, Errors.TEMPLATE_PARSE_ERROR)
            e.printStackTrace()
            return
        } catch (e: IOException) {
            callInternalError(exchange)
            e.printStackTrace()
            return
        }

        exchange.responseSender.send(writer.toString())
    }

    protected fun sendSafePage(exchange: HttpServerExchange, template: Template) {
        exchange.responseHeaders.put(Headers.CONTENT_TYPE, "text/html")
        exchange.responseSender.send(template.toString())
    }

    protected fun error(exchange: HttpServerExchange, error: Errors) {
        error(exchange, error.message)
    }

    protected fun error(exchange: HttpServerExchange, reason: String?) {
        exchange.responseHeaders.put(Headers.CONTENT_TYPE, "text/html")

        val placeholders = HashMap<String, Any>()
        val writer = StringWriter()

        placeholders["message"] = reason ?: ""

        try {
            val template = getTemplate("error")
            template!!.process(placeholders, writer)
        } catch (e: Exception) {
            e.printStackTrace()
            callInternalError(exchange)
            return
        }

        exchange.responseSender.send(writer.toString())
    }

    protected fun callInternalError(exchange: HttpServerExchange) {
        exchange.responseHeaders.put(Headers.CONTENT_TYPE, "text/html")
        exchange.responseSender.send(ERROR_PAGE_CONTEXT)
    }

    companion object {
        fun getGetParam(key: String, exchange: HttpServerExchange): String? {
            return (exchange.queryParameters as Map<String, Deque<String>>).getOrDefault(key, UserExchangeAuthUtil.emptyDeque).poll()
        }

        fun getPostParam(key: String, exchange: HttpServerExchange): String? {
            return try {
                exchange
                        .getAttachment(FormDataParser.FORM_DATA)
                        .getFirst(key)
                        .value
            } catch (e: NullPointerException) {
                null
            }
        }

        fun getPathParam(exchange: HttpServerExchange): List<String>? {
            var list = exchange.relativePath.split("/")
            if (list.size < 2) return null
            if (list[0].isBlank()) list = list.subList(1, list.size)
            if (list.isEmpty() || list[0].isBlank()) return null
            return list
        }

        private val ERROR_PAGE_CONTEXT = "<html><head><meta charset=\"utf-8\"><link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\"><title>INTERNAL SERVER ERROR</title></head><body><div class=\"container\"><center><h1>" + Errors.INTERNAL_ERROR.message + "</h1><p>Report to the administrator about this error.</p><p> (╯°□°）╯︵ ┻━┻</p></center></div></body></html>"
    }
}
