package com.demkom58.liteedge.engine.core.managers.resource

import com.demkom58.liteedge.engine.core.managers.user.unit.User
import java.io.File

interface ResourceManager {

    fun getFile(path: String): File
    fun getAvatarFile(name: String): File
    fun getAvatarLink(user: User): String
    fun downloadGeoDatabase(): File

}