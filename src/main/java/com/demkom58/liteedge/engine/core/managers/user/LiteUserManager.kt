package com.demkom58.liteedge.engine.core.managers.user

import com.demkom58.liteedge.engine.core.Core
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.engine.database.DatabaseRequester
import com.demkom58.liteedge.util.StringUtil
import java.sql.Connection
import java.sql.ResultSet

class LiteUserManager(val core: Core, val requester: DatabaseRequester) : UserManager {

    override fun getUser(login: String): User? {
        val isMail = isValidEmail(login)
        if (!isMail && !isValidNickname(login)) return null

        val sqlReq: String = if (isMail) "SELECT * FROM `le_users` WHERE `email` LIKE ?" else "SELECT * FROM `le_users` WHERE `nick` LIKE ?"

        val connection = requester.connection
        val stmt = connection.prepareStatement(sqlReq)

        stmt.setString(1, login)
        val result = stmt.executeQuery()

        val user: User?

        if (result.next()) {
            user = readUserFromResultSet(result)

            result.close()
            stmt.close()
            connection.close()
        } else return null
        return user
    }

    override fun getUser(id: Int): User? {
        val connection = requester.connection
        val stmt = connection.prepareStatement("SELECT * FROM `le_users` WHERE `id` LIKE ?")

        stmt.setInt(1, id)
        val result = stmt.executeQuery()

        val user: User

        if (result.next()) {
            user = readUserFromResultSet(result)

            result.close()
            stmt.close()
            connection.close()
        } else return null
        return user
    }

    private fun readUserFromResultSet(result: ResultSet): User {
        val id: Int = result.getInt("id")
        val userID: String? = result.getString("user_id")
        val sessionHash: String = result.getString("session_hash")
        val passwordHash: String = result.getString("password_hash")
        val lastIP: String = result.getString("last_ip")
        val userAgent: String = result.getString("user_agent")
        val timeZone: String = result.getString("time_zone")
        val registerDate: String = result.getString("register_date")
        val avatar: String? = result.getString("avatar")
        val email: String = result.getString("email")
        val nickname: String = result.getString("nick")
        val phone: String? = result.getString("phone")
        val realname: String? = result.getString("realname")
        val about: String? = result.getString("about")
        val rating: Int = result.getInt("rating")
        val banned: String? = result.getString("banned")
        val userGroup: Int = result.getInt("user_group")
        val newsPosted: Int = result.getInt("news_posted_num")
        val threadsPosted: Int = result.getInt("threads_posted_num")
        val newsCommented: Int = result.getInt("news_commented_num")
        val threadsCommented: Int = result.getInt("threads_commented_num")
        val profileComments: String? = result.getString("profile_comments")

        return User(
                id, userID, sessionHash, passwordHash, lastIP, userAgent, timeZone, registerDate, avatar, email,
                nickname, phone, realname, about, rating, banned, userGroup, newsPosted, threadsPosted, newsCommented,
                threadsCommented, profileComments
        )
    }

    override fun createUser(email: String, nick: String, sessionHash: String, passwordHash: String, lastIP: String, userAgent: String, timeZone: String): Boolean {
        if (existUserByNick(nick)) return false
        if (existUserByEMail(email)) return false

        val connection: Connection = requester.connection
        val statement = connection.prepareStatement(
                "INSERT INTO `le_users` (`session_hash`, `password_hash`, `last_ip`, `user_agent`, `time_zone`, `email`, `nick`) VALUES (?, ?, ?, ?, ?, ?, ?)")

        statement.setString(1, sessionHash)
        statement.setString(2, passwordHash)
        statement.setString(3, lastIP)
        statement.setString(4, userAgent)
        statement.setString(5, timeZone)
        statement.setString(6, email)
        statement.setString(7, nick)

        val result: Boolean = statement.execute()

        connection.close()
        statement.close()

        return result
    }

    override fun createUser(user: User): Boolean {
        if (existUserByNick(user.nick)) return false
        if (existUserByEMail(user.email)) return false
        if (existUserByID(user.id)) return false

        val connection: Connection = requester.connection
        val statement = connection.prepareStatement(
                "INSERT INTO `le_users` (`id`, `user_id`, `session_hash`, `password_hash`, `last_ip`, `user_agent`, `time_zone`, `register_date`, `avatar`, `email`, `nick`, `phone`, `realname`, `about`, `rating`, `banned`, `user_group`, `news_posted_num`, `threads_posted_num`, `news_commented_num`, `threads_commented_num`, `profile_comments`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")

        statement.setInt(1, user.id)
        statement.setString(2, user.userID)
        statement.setString(3, user.sessionHash)
        statement.setString(4, user.passwordHash)
        statement.setString(5, user.lastIP)
        statement.setString(6, user.userAgent)
        statement.setString(7, user.timeZone)
        statement.setString(8, user.registerDate)
        statement.setString(9, user.avatar)
        statement.setString(10, user.email)
        statement.setString(11, user.nick)
        statement.setString(12, user.phone)
        statement.setString(13, user.realname)
        statement.setString(14, user.about)
        statement.setInt(15, user.rating)
        statement.setString(16, user.banned)
        statement.setInt(17, user.userGroup)
        statement.setInt(18, user.newsPosted)
        statement.setInt(19, user.threadsPosted)
        statement.setInt(10, user.newsCommented)
        statement.setInt(21, user.threadsCommented)
        statement.setString(22, user.profileComments)


        val result: Boolean = statement.execute()

        connection.close()
        statement.close()

        return result
    }


    override fun updateUser(user: User): Boolean {
        if (!existUserByNick(user.nick)) return false
        if (!existUserByEMail(user.email)) return false

        val connection: Connection = requester.connection
        val statement = connection.prepareStatement(
                "UPDATE `le_users` SET `user_id` = ?, `session_hash` = ?, `password_hash` = ?, `last_ip` = ?, `user_agent` = ?, `time_zone` = ?, `register_date` = ?, `avatar` = ?, `email` = ?, `nick` = ?, `phone` = ?, `realname` = ?, `about` = ?, `rating` = ?, `banned` = ?, `user_group` = ?, `news_posted_num` = ?, `threads_posted_num` = ?, `news_commented_num` = ?, `threads_commented_num` = ?, `profile_comments` = ? WHERE `le_users`.`id` = ?;")

        statement.setString(1, user.userID)
        statement.setString(2, user.sessionHash)
        statement.setString(3, user.passwordHash)
        statement.setString(4, user.lastIP)
        statement.setString(5, user.userAgent)
        statement.setString(6, user.timeZone)
        statement.setString(7, user.registerDate)
        statement.setString(8, user.avatar)
        statement.setString(9, user.email)
        statement.setString(10, user.nick)
        statement.setString(11, user.phone)
        statement.setString(12, user.realname)
        statement.setString(13, user.about)
        statement.setInt(14, user.rating)
        statement.setString(15, user.banned)
        statement.setInt(16, user.userGroup)
        statement.setInt(17, user.newsPosted)
        statement.setInt(18, user.threadsPosted)
        statement.setInt(19, user.newsCommented)
        statement.setInt(20, user.threadsCommented)
        statement.setString(21, user.profileComments)
        statement.setInt(22, user.id)

        val result: Boolean = statement.executeUpdate() >= 1

        statement.close()
        connection.close()

        return result
    }

    override fun existUserByLogin(login: String): Boolean {
        val isMail = isValidEmail(login)
        if (!isMail && !isValidNickname(login)) return false
        return if (isMail) existUserByEMail(login) else existUserByNick(login)
    }

    override fun existUserByNick(nick: String): Boolean {
        val connection = requester.connection
        val stmt = connection.prepareStatement("SELECT * FROM `le_users` WHERE `nick` LIKE ?")

        stmt.setString(1, nick)
        val resultSet = stmt.executeQuery()

        val result: Boolean = resultSet.next()

        resultSet.close()
        stmt.close()
        connection.close()

        return result
    }

    override fun existUserByEMail(email: String): Boolean {
        val connection = requester.connection
        val stmt = connection.prepareStatement("SELECT * FROM `le_users` WHERE `email` LIKE ?")

        stmt.setString(1, email)
        val resultSet = stmt.executeQuery()

        val result: Boolean = resultSet.next()

        resultSet.close()
        stmt.close()
        connection.close()

        return result
    }

    override fun existUserByID(id: Int): Boolean {
        val connection = requester.connection
        val stmt = connection.prepareStatement("SELECT * FROM `le_users` WHERE `id` LIKE ?")

        stmt.setInt(1, id)
        val resultSet = stmt.executeQuery()

        val result: Boolean = resultSet.next()

        resultSet.close()
        stmt.close()
        connection.close()

        return result
    }

    override fun isValidEmail(email: String?): Boolean {
        return try {
            StringUtil.checkEmail(email)
        } catch (e: Exception) {
            false
        }
    }

    override fun isValidNickname(nick: String?): Boolean {
        return try {
            StringUtil.checkNick(nick)
        } catch (e: Exception) {
            false
        }
    }

    override fun isValidPassword(password: String?): Boolean {
        return try {
            StringUtil.checkPassword(password)
        } catch (e: Exception) {
            false
        }
    }

    override fun getSessionHash(email: String, nick: String, passHash: String, timeZone: String, userAgent: String): String {
        return StringUtil.getMD5(email + nick + passHash + timeZone + userAgent)
    }

}