package com.demkom58.liteedge.engine.core.managers.page.exact

import com.demkom58.liteedge.engine.core.managers.page.root.Page
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.util.UserExchangeAuthUtil
import freemarker.template.Template
import io.undertow.server.HttpHandler
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers
import io.undertow.util.StatusCodes
import java.util.*

class RegisterPage(path: String, templateName: String) : Page(path, templateName) {

    override fun handler(): HttpHandler {
        return makeHandler()
    }

    override fun handle(exchange: HttpServerExchange, template: Template) {
        val value = getGetParam("action", exchange)
        when (value) {
            "register" -> processRegister(exchange)
            else -> showPage(exchange, template)
        }
    }

    private fun processRegister(exchange: HttpServerExchange) {
        val user: User =
                try {
                    UserExchangeAuthUtil.registerUser(
                            exchange,
                            getGetParam("email", exchange),
                            getGetParam("nick", exchange),
                            getGetParam("password", exchange))

                } catch (e: Exception) {
                    exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
                    exchange.queryParameters.clear()
                    exchange.responseHeaders.put(Headers.LOCATION, "/register")
                    exchange.endExchange()
                    return
                }

        val placeholders = HashMap<String, Any>()

        placeholders["head"] = "Done."
        placeholders["content"] = "${user.nick}, you are successfully registered."

        exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
        exchange.queryParameters.clear()
        exchange.responseHeaders.put(Headers.LOCATION, "/profile/" + user.id)
        exchange.endExchange()
    }

    private fun showPage(exchange: HttpServerExchange, template: Template) {
        sendSafePage(exchange, template)
    }

    override fun handle(exchange: HttpServerExchange, template: Template, user: User) {
        exchange.statusCode = StatusCodes.MOVED_PERMANENTLY
        exchange.queryParameters.clear()
        exchange.responseHeaders.put(Headers.LOCATION, "/profile/" + user.id)
        exchange.endExchange()
    }
}
