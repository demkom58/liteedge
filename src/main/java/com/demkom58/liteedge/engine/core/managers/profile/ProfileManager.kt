package com.demkom58.liteedge.engine.core.managers.profile

import com.demkom58.liteedge.engine.core.managers.user.unit.User
import java.io.File

interface ProfileManager {

    @Throws(UnsupportedOperationException::class)
    fun updateAvatar(image: File, user: User)

}