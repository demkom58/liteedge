package com.demkom58.liteedge.engine.core.configurations

class DatabaseConfiguration(
        val className: String,
        val connectString: String,
        val database: String,
        val args: String,
        val login: String,
        val password: String
)
