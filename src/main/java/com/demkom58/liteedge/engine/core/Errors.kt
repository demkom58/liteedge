package com.demkom58.liteedge.engine.core


enum class Errors constructor(val message: String) {
    INTERNAL_ERROR("INTERNAL SERVER ERROR"),
    NO_TEMPLATE_FOUND("Template for this page wasn't found!"),
    TEMPLATE_PARSE_ERROR("Template loading error."),
    DATABASE_ERROR("An error occurred while working with the database."),
    BAD_PARAMS("Bad request, try to correct it."),
    AUTH_ERROR("Something incorrect, check your data.")
}
