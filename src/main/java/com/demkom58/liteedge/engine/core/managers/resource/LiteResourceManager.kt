package com.demkom58.liteedge.engine.core.managers.resource

import com.demkom58.liteedge.engine.core.Core
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.util.ClassPathExtractUtil
import com.demkom58.liteedge.util.StringUtil
import org.rauschig.jarchivelib.ArchiverFactory
import java.io.File
import java.net.URL
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class LiteResourceManager(core: Core) : ResourceManager {

    init {
        val resources = File(StringUtil.getResourceDirPath())
        if (!resources.exists()) {
            println("Extracting resources...")
            ClassPathExtractUtil.export("/resources/", StringUtil.getResourceDirPath())
        }
    }

    override fun getFile(path: String): File {
        return File(StringUtil.getExecuteDirPath() + "LiteEdge/resources/$path")
    }

    override fun getAvatarFile(name: String): File {
        return getFile("a/$name")
    }

    override fun getAvatarLink(user: User): String {
        if (user.avatar == null) return "/r/a/default.png"
        if (!File(StringUtil.getAvatarsDirPath() + user.id + ".${user.avatar}").exists()) return "/r/a/default.png"
        return "/r/a/${user.id}.${user.avatar}"
        //TODO: detect need format
    }

    override fun downloadGeoDatabase(): File {
        val geoIpBase: File

        val tarBase: File
        val unzippedFolder: File

        val dir = StringUtil.getGeoIPDirPath()

        val link = "http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz"
        val tar = dir + "GeoLite2-City.tar.gz"
        val done = dir + "GeoLite2-City.mmdb"
        val unzipped = dir + "unzipped"

        geoIpBase = File(done)
        if (geoIpBase.exists()) return geoIpBase
        else println("Downloading GeoIP database...")

        unzippedFolder = File(unzipped)
        tarBase = File(tar)

        val website = URL(link)

        tarBase.mkdirs()
        website.openStream().use({ `in` -> Files.copy(`in`, tarBase.toPath(), StandardCopyOption.REPLACE_EXISTING) })

        ArchiverFactory.createArchiver("tar", "gz").extract(tarBase, unzippedFolder)
        for (folder in unzippedFolder.listFiles()) {
            if (folder.isDirectory) {
                for (db in folder.listFiles()) {
                    if (db.name == "GeoLite2-City.mmdb") {
                        Files.copy(db.toPath(), geoIpBase.toPath(), StandardCopyOption.REPLACE_EXISTING)
                        break
                    }
                }
                break
            }
        }
        unzippedFolder.deleteRecursively()
        tarBase.delete()
        return geoIpBase
    }


}