package com.demkom58.liteedge.engine.exceptions

class InvalidUserDataException(reason: String) : UserException(reason)
