package com.demkom58.liteedge.engine.exceptions

import com.demkom58.liteedge.engine.core.Errors


class DatabaseException(reason: String) : Exception(reason) {
    constructor() : this(Errors.DATABASE_ERROR.message)
}