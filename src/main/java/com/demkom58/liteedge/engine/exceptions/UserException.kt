package com.demkom58.liteedge.engine.exceptions

open class UserException(reason: String) : Exception(reason)
