package com.demkom58.liteedge.engine.exceptions

class NoSuchUserException(reason: String) : UserException(reason) {
    constructor() : this("This user doesn't exist!")
}