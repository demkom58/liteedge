package com.demkom58.liteedge.engine.exceptions

class InvalidPasswordException(reason: String) : UserException(reason)