package com.demkom58.liteedge.engine.exceptions

class UserAlreadyExistException(reason: String) : UserException(reason)