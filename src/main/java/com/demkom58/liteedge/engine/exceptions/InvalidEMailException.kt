package com.demkom58.liteedge.engine.exceptions

class InvalidEMailException(reason: String) : UserException(reason)