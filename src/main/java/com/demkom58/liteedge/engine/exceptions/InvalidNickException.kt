package com.demkom58.liteedge.engine.exceptions


class InvalidNickException(reason: String) : UserException(reason)