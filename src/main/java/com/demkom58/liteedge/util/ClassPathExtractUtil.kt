package com.demkom58.liteedge.util

import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.JarURLConnection

object ClassPathExtractUtil {

    @Throws(IOException::class)
    fun export(sourceDirectory: String, writeDirectory: String) {
        File(writeDirectory).mkdirs()
        extract(sourceDirectory, writeDirectory)
    }

    @Throws(IOException::class)
    private fun extract(sourceDirectory: String, writeDirectory: String) {
        val dirURL = javaClass.getResource(sourceDirectory)
        val path = sourceDirectory.substring(1)

        if (dirURL != null && dirURL.protocol == "jar") {
            val jarConnection = dirURL.openConnection() as JarURLConnection
            val jar = jarConnection.jarFile

            val entries = jar.entries()

            while (entries.hasMoreElements()) {
                val entry = entries.nextElement()
                val name = entry.name

                if (!name.startsWith(path)) continue

                val entryTail = name.substring(path.length)
                val f = File(writeDirectory + File.separator + entryTail)

                if (entry.isDirectory) f.mkdir()
                else {
                    val `is` = jar.getInputStream(entry)
                    val os = BufferedOutputStream(FileOutputStream(f))
                    val buffer = ByteArray(4096)
                    var readCount: Int = `is`.read(buffer)
                    while (readCount > 0) {
                        os.write(buffer, 0, readCount)
                        readCount = `is`.read(buffer)
                    }
                    os.close()
                    `is`.close()
                }
            }

        } else if (dirURL == null) throw IllegalStateException("Can't find $sourceDirectory on the classpath.")
        else throw IllegalStateException("Don't know how to handle extracting from $dirURL.")

    }
}