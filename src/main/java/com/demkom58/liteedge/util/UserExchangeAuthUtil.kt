package com.demkom58.liteedge.util

import com.demkom58.liteedge.LiteEdge
import com.demkom58.liteedge.engine.core.managers.user.UserManager
import com.demkom58.liteedge.engine.core.managers.user.unit.User
import com.demkom58.liteedge.engine.exceptions.*
import io.undertow.server.HttpServerExchange
import io.undertow.server.handlers.CookieImpl
import java.sql.SQLException
import java.util.*

object UserExchangeAuthUtil {
    val emptyDeque = ArrayDeque<String>(0)

    fun logoutSession(exchange: HttpServerExchange) {
        exchange.setResponseCookie(CookieImpl("userID").setValue("0").setMaxAge(0).setPath("/"))
        exchange.setResponseCookie(CookieImpl("sessionHash").setValue("0").setMaxAge(0).setPath("/"))
    }

    @Throws(InvalidUserDataException::class, NoSuchUserException::class, InvalidPasswordException::class)
    fun authExchange(exchange: HttpServerExchange, login: String?, password: String?): User {
        var user: User? = getUserBySession(exchange)
        if (user == null) user = createUserSession(exchange, login, password)
        return user
    }

    @Throws(InvalidUserDataException::class, NoSuchUserException::class, InvalidPasswordException::class)
    fun createUserSession(exchange: HttpServerExchange, login: String?, password: String?): User {
        val userManager: UserManager = LiteEdge.instance.liteCore.userManager

        val id: String
        val nickname: String
        val email: String

        if (login == null || password == null) throw InvalidUserDataException("Not enough data.")
        val isValidMail = userManager.isValidEmail(login)
        val isValidNick = userManager.isValidNickname(login)
        val isValidPassword = userManager.isValidPassword(password)

        if (!isValidMail && !isValidNick) throw InvalidUserDataException("Your login is invalid!")
        if (!isValidPassword) throw InvalidUserDataException("Your password is invalid!")


        val user: User = userManager.getUser(login) ?: throw NoSuchUserException()

        val passwordHash = StringUtil.getMD5(password)
        val ip = exchange.sourceAddress.address.hostAddress
        val agent = exchange.requestHeaders.getFirst("User-Agent")
        val timezone = StringUtil.getTimeZone(ip)
        val sessionHash: String

        id = user.id.toString()
        nickname = user.nick
        email = user.email

        sessionHash = userManager.getSessionHash(email, nickname, passwordHash, timezone, agent)
        if (user.passwordHash == passwordHash) {
            val cookieMaxAge = 15552000
            exchange.setResponseCookie(CookieImpl("userID").setMaxAge(cookieMaxAge).setValue(id).setPath("/"))
            exchange.setResponseCookie(CookieImpl("sessionHash").setMaxAge(cookieMaxAge).setValue(sessionHash).setPath("/"))

            user.sessionHash = sessionHash
            user.userAgent = agent
            user.lastIP = ip
            user.timeZone = timezone

            userManager.updateUser(user)
            return user
        } else throw InvalidPasswordException("Please, try another password!")
    }

    @Throws(NoSuchUserException::class)
    fun getUserBySession(exchange: HttpServerExchange): User {
        val userManager = LiteEdge.instance.liteCore.userManager

        if (!exchange.requestCookies.containsKey("userID") || !exchange.requestCookies.containsKey("sessionHash")) throw NoSuchUserException()
        val cookieID: Int = Integer.parseInt(exchange.requestCookies["userID"]!!.value)
        val databaseUser: User = userManager.getUser(cookieID) ?: throw NoSuchUserException()

        val cookieSessionHash: String = exchange.requestCookies["sessionHash"]!!.value
        val databaseSessionHash: String = databaseUser.sessionHash
        val userSessionHash: String = userManager.getSessionHash(databaseUser.email, databaseUser.nick, databaseUser.passwordHash, StringUtil.getTimeZone(exchange.sourceAddress.address.hostAddress), exchange.requestHeaders.getFirst("User-Agent"))

        return if ((userSessionHash == databaseSessionHash) && (databaseSessionHash == cookieSessionHash)) databaseUser else throw NoSuchUserException()

    }

    @Throws(InvalidUserDataException::class, InvalidEMailException::class, InvalidNickException::class,
            InvalidPasswordException::class, UserAlreadyExistException::class, DatabaseException::class)
    fun registerUser(exchange: HttpServerExchange, email: String?, nick: String?, password: String?): User {
        val user: User
        val userManager: UserManager = LiteEdge.instance.liteCore.userManager

        StringUtil.checkData(email!!, nick!!, password!!)

        try {
            if (userManager.existUserByLogin(email)) throw UserAlreadyExistException("User with this email already exist.")
            if (userManager.existUserByLogin(nick)) throw UserAlreadyExistException("User with this nick already exist.")

            val passwordHash = StringUtil.getMD5(password)
            val ip = exchange.sourceAddress.address.hostAddress
            val agent = exchange.requestHeaders.getFirst("User-Agent")
            val timezone = StringUtil.getTimeZone(ip)
            val sessionHash = userManager.getSessionHash(email, nick, passwordHash, timezone, agent)

            userManager.createUser(email, nick, sessionHash, passwordHash, ip, agent, timezone)
            user = userManager.getUser(nick)!!

            val cookieMaxAge = 15552000
            exchange.setResponseCookie(CookieImpl("userID").setMaxAge(cookieMaxAge).setValue(user.id.toString()).setPath("/"))
            exchange.setResponseCookie(CookieImpl("sessionHash").setMaxAge(cookieMaxAge).setValue(sessionHash).setPath("/"))
        } catch (e: SQLException) {
            e.printStackTrace()
            throw DatabaseException()
        }
        return user
    }
}