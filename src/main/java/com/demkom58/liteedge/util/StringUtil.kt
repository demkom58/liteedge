package com.demkom58.liteedge.util

import com.demkom58.liteedge.LiteEdge
import com.demkom58.liteedge.engine.exceptions.InvalidEMailException
import com.demkom58.liteedge.engine.exceptions.InvalidNickException
import com.demkom58.liteedge.engine.exceptions.InvalidPasswordException
import com.demkom58.liteedge.engine.exceptions.InvalidUserDataException
import com.maxmind.geoip2.exception.GeoIp2Exception
import org.apache.commons.validator.routines.EmailValidator
import java.io.File

import java.io.IOException
import java.math.BigInteger
import java.net.InetAddress
import java.net.URLDecoder
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.regex.Pattern

object StringUtil {

    fun getMD5(text: String): String {
        val m: MessageDigest

        try {
            m = MessageDigest.getInstance("MD5")
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
            return "00000000000000000000000000000000"
        }

        m.reset()
        m.update(text.toByteArray())
        val digest = m.digest()
        val bigInt = BigInteger(1, digest)
        val hashText = StringBuilder(bigInt.toString(16))

        while (hashText.length < 32) {
            hashText.insert(0, "0")
        }
        return hashText.toString()
    }

    fun getCity(ip: String): String {
        return try {
            val ipAddress = InetAddress.getByName(ip)
            return LiteEdge.instance.liteCore.geoIpReader.city(ipAddress).city.name
        } catch (e: GeoIp2Exception) {
            "unknown"
        } catch (e: IOException) {
            "unknown"
        }
    }

    fun getTimeZone(ip: String): String {
        return try {
            val ipAddress = InetAddress.getByName(ip)
            val response = LiteEdge.instance.liteCore.geoIpReader.city(ipAddress)
            response.location.timeZone
        } catch (e: GeoIp2Exception) {
            "unknown"
        } catch (e: IOException) {
            "unknown"
        }
    }

    @Throws(InvalidUserDataException::class, InvalidEMailException::class, InvalidNickException::class, InvalidPasswordException::class)
    fun checkData(email: String?, nick: String?, password: String?): Boolean {
        checkEmail(email)
        checkNick(nick)
        checkPassword(password)
        return true
    }

    @Throws(InvalidUserDataException::class, InvalidEMailException::class)
    fun checkEmail(email: String?): Boolean {
        if (email == null) throw InvalidUserDataException("Not enough Data.")
        if (!EmailValidator.getInstance().isValid(email)) throw InvalidEMailException("Please try another EMail.")
        if (email.length !in 5..30) throw InvalidEMailException("Email length is invalid.")
        return true
    }

    @Throws(InvalidUserDataException::class, InvalidNickException::class)
    fun checkNick(nick: String?): Boolean {
        if (nick == null) throw InvalidUserDataException("Not enough Data.")
        if (nick.length !in 2..14) throw InvalidNickException("Size of your nick invalid.")
        if (Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE).matcher(nick).find()) throw InvalidNickException("Your nick name contains not allowed symbols.")
        return true
    }

    @Throws(InvalidUserDataException::class, InvalidPasswordException::class)
    fun checkPassword(password: String?): Boolean {
        if (password == null) throw InvalidUserDataException("Not enough Data.")
        if (password.length !in 4..16) throw InvalidPasswordException("Please try another Password.")
        return true
    }

    fun getExecuteDirPath(): String {
        return URLDecoder.decode(File(LiteEdge::class.java.protectionDomain.codeSource.location.path).parent + "/", "UTF-8")
    }

    fun getRootDirPath(): String {
        return getExecuteDirPath() + "LiteEdge/"
    }

    fun getResourceDirPath(): String {
        return getRootDirPath() + "resources/"
    }

    fun getConfigurationsDirPath(): String {
        return getRootDirPath() + "configurations/"
    }

    fun getGeoIPDirPath(): String {
        return getRootDirPath() + "geoIP/"
    }

    fun getAvatarsDirPath(): String {
        return getResourceDirPath() + "a/"
    }
}
