package com.demkom58.liteedge

import com.demkom58.liteedge.engine.EngineFactory
import com.demkom58.liteedge.engine.core.Core
import com.demkom58.liteedge.engine.core.LiteCore
import com.demkom58.liteedge.engine.core.managers.page.PageManager
import com.demkom58.liteedge.engine.core.managers.page.exact.AuthPage
import com.demkom58.liteedge.engine.core.managers.page.exact.ProfilePage
import com.demkom58.liteedge.engine.core.managers.page.exact.RegisterPage
import com.demkom58.liteedge.engine.core.managers.page.exact.RootPage
import com.demkom58.liteedge.engine.core.managers.template.TemplateManager
import io.undertow.Undertow

class LiteEdge {
    private var undertow: Undertow? = null
    private var running = false
    val liteCore: Core

    /**
     * Register templates methods.
     *
     * Too add new template, you must change this method,
     * or just add new template to hashmap with templates
     * and restart engine.
     *
     * Yes, its hardcoded...
     */
    private fun registerTemplates() {
        val tm: TemplateManager = liteCore.templateManager

        tm.registerTemplate("detected", "errors/detected.html")
        tm.registerTemplate("success", "auth/success.html")

        tm.registerTemplate("root", "root.html")
        tm.registerTemplate("auth", "auth/auth.html")
        tm.registerTemplate("register", "auth/register.html")

        tm.registerTemplate("profile", "user/profile.html")

        tm.registerTemplate("error", "errors/error.html")
    }

    /**
     * Register pages method.
     *
     * To add new page, you must change this method,
     * or just add new page in EngineFactory. But to
     * apply new pages, you must restart engine.
     *
     * Yes, its hardcoded too...
     */
    private fun registerPages() {
        val pm: PageManager = liteCore.pageManager
        pm.registerExactPage(RootPage("/", "root"))

        pm.registerExactPage(AuthPage("login", "auth"))
        pm.registerExactPage(RegisterPage("register", "register"))

        pm.registerPrefixPage(ProfilePage("profile", "profile"))
    }

    init {
        instance = this
        liteCore = LiteCore()

        //Prepare all for building and starting.
        registerTemplates()
        registerPages()

        //Start building then start engine.
        start()
    }

    /**
     * Rebuild and start engine methods.
     *
     * If you added new pages after last start,
     * on restart new pages will be applied.
     *
     * Don't forget stop engine before it. :D
     */
    fun start() {
        rebuild()
        if (!running) undertow!!.start()
        running = true
    }

    /**
     * Stop engine method.
     */
    fun stop() {
        if (running) undertow!!.stop()
        running = false
    }

    /**
     * Rebuild engine methods.
     */
    private fun rebuild() {
        undertow = EngineFactory.getBuilder().build()
    }

    companion object {
        lateinit var instance: LiteEdge

        @JvmStatic
        fun main(args: Array<String>) {
            LiteEdge()
        }
    }
}
