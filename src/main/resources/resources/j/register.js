if (typeof kotlin === 'undefined') {
    throw new Error("Error loading module 'KotlinJS TEST'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'KotlinJS TEST'.");
}
this['KotlinJS TEST'] = function (_, Kotlin) {
    'use strict';
    var ensureNotNull = Kotlin.ensureNotNull;
    var throwCCE = Kotlin.throwCCE;
    var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_ww73n8$;
    var println = Kotlin.kotlin.io.println_s8jyv4$;
    var Unit = Kotlin.kotlin.Unit;
    var contains = Kotlin.kotlin.text.contains_li3zpu$;
    var signupAlert;
    var nickInput;
    var emailInput;
    var passwordInput;
    var problems;

    function main$lambda(it) {
        checkNick();
        return Unit;
    }

    function main$lambda_0(it) {
        checkEmail();
        return Unit;
    }

    function main$lambda_1(it) {
        checkPassword();
        return Unit;
    }

    function main(args) {
        println("Hello Guest!\nIt's LiteEdge engine.\nHope you will enjoy :)");
        nickInput.addEventListener('change', main$lambda);
        emailInput.addEventListener('change', main$lambda_0);
        passwordInput.addEventListener('change', main$lambda_1);
    }

    function checkNick() {
        var nick = nickInput.value;
        var message = '<p>Your nick is incorrect!<\/p>';
        var validNick_0 = validNick(nick);
        if (!validNick_0) {
            if (!contains(signupAlert.innerHTML, message))
                problems.add_11rb$(message);
        }
        else
            problems.remove_11rb$(message);
        renderError();
    }

    function checkEmail() {
        var email = emailInput.value;
        var message = '<p>Your email is incorrect!<\/p>';
        var validEmail_0 = validEmail(email);
        if (!validEmail_0) {
            if (!contains(signupAlert.innerHTML, message))
                problems.add_11rb$(message);
        }
        else
            problems.remove_11rb$(message);
        renderError();
    }

    function checkPassword() {
        var password = passwordInput.value;
        var message = '<p>Your password is incorrect!<\/p>';
        var validPassword_0 = validPassword(password);
        if (!validPassword_0) {
            if (!contains(signupAlert.innerHTML, message))
                problems.add_11rb$(message);
        }
        else
            problems.remove_11rb$(message);
        renderError();
    }

    function renderError() {
        var tmp$;
        setErrorText('');
        if (problems.size === 0) {
            hideErrorBox();
            return;
        }
        tmp$ = problems.iterator();
        while (tmp$.hasNext()) {
            var problem = tmp$.next();
            if (getErrorText().length === 0)
                setErrorText(problem);
            else
                setErrorText(getErrorText() + '\n' + problem);
        }
        showErrorBox();
    }

    function showErrorBox() {
        signupAlert.removeAttribute('style');
    }

    function hideErrorBox() {
        signupAlert.setAttribute('style', 'display:none');
    }

    function setErrorText(string) {
        signupAlert.innerHTML = string;
    }

    function getErrorText() {
        return signupAlert.innerHTML;
    }

    var Regex_init = Kotlin.kotlin.text.Regex_init_61zpoe$;

    function validEmail(email) {
        var tmp$;
        tmp$ = email.length;
        if (!(5 <= tmp$ && tmp$ <= 30))
            return false;
        var regular = Regex_init('(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])');
        return regular.matches_6bul2c$(email);
    }

    function validNick(nick) {
        var tmp$;
        tmp$ = nick.length;
        if (!(2 <= tmp$ && tmp$ <= 14))
            return false;
        var regular = Regex_init('^[a-zA-Z0-9_.-]*$');
        return regular.matches_6bul2c$(nick);
    }

    function validPassword(password) {
        var tmp$;
        tmp$ = password.length;
        if (!(4 <= tmp$ && tmp$ <= 16))
            return false;
        return true;
    }

    var package$com = _.com || (_.com = {});
    var package$demkom58 = package$com.demkom58 || (package$com.demkom58 = {});
    var package$kotlinjstest = package$demkom58.kotlinjstest || (package$demkom58.kotlinjstest = {});
    Object.defineProperty(package$kotlinjstest, 'signupAlert', {
        get: function () {
            return signupAlert;
        }
    });
    Object.defineProperty(package$kotlinjstest, 'nickInput', {
        get: function () {
            return nickInput;
        }
    });
    Object.defineProperty(package$kotlinjstest, 'emailInput', {
        get: function () {
            return emailInput;
        }
    });
    Object.defineProperty(package$kotlinjstest, 'passwordInput', {
        get: function () {
            return passwordInput;
        }
    });
    Object.defineProperty(package$kotlinjstest, 'problems', {
        get: function () {
            return problems;
        }
    });
    package$kotlinjstest.main_kand9s$ = main;
    signupAlert = ensureNotNull(document.getElementById('signupalert'));
    var tmp$, tmp$_0, tmp$_1;
    nickInput = ensureNotNull((tmp$ = document.getElementById('nick-input')) == null || Kotlin.isType(tmp$, HTMLInputElement) ? tmp$ : throwCCE());
    emailInput = ensureNotNull((tmp$_0 = document.getElementById('email-input')) == null || Kotlin.isType(tmp$_0, HTMLInputElement) ? tmp$_0 : throwCCE());
    passwordInput = ensureNotNull((tmp$_1 = document.getElementById('password-input')) == null || Kotlin.isType(tmp$_1, HTMLInputElement) ? tmp$_1 : throwCCE());
    problems = ArrayList_init();
    main([]);
    Kotlin.defineModule('KotlinJS TEST', _);
    return _;
}(typeof this['KotlinJS TEST'] === 'undefined' ? {} : this['KotlinJS TEST'], kotlin);
