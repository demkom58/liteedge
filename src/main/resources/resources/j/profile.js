if (typeof kotlin === 'undefined') {
    throw new Error("Error loading module 'KotlinJS TEST'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'KotlinJS TEST'.");
}
this['KotlinJS TEST'] = function (_, Kotlin) {
    'use strict';
    var throwCCE = Kotlin.throwCCE;
    var println = Kotlin.kotlin.io.println_s8jyv4$;
    var ensureNotNull = Kotlin.ensureNotNull;
    var Unit = Kotlin.kotlin.Unit;
    var avatarInput;
    var form;

    function main$lambda(it) {
        ensureNotNull(form).submit();
        return Unit;
    }

    function main(args) {
        println("Hello Guest!\nIt's LiteEdge engine.\nHope you will enjoy :)");
        ensureNotNull(avatarInput).addEventListener('change', main$lambda);
    }

    var package$com = _.com || (_.com = {});
    var package$demkom58 = package$com.demkom58 || (package$com.demkom58 = {});
    var package$kotlinjstest = package$demkom58.kotlinjstest || (package$demkom58.kotlinjstest = {});
    Object.defineProperty(package$kotlinjstest, 'avatarInput', {
        get: function () {
            return avatarInput;
        }
    });
    Object.defineProperty(package$kotlinjstest, 'form', {
        get: function () {
            return form;
        }
    });
    package$kotlinjstest.main_kand9s$ = main;
    var tmp$, tmp$_0;
    avatarInput = (tmp$ = document.getElementById('avatar-input')) == null || Kotlin.isType(tmp$, HTMLInputElement) ? tmp$ : throwCCE();
    form = (tmp$_0 = document.getElementById('form')) == null || Kotlin.isType(tmp$_0, HTMLFormElement) ? tmp$_0 : throwCCE();
    main([]);
    Kotlin.defineModule('KotlinJS TEST', _);
    return _;
}(typeof this['KotlinJS TEST'] === 'undefined' ? {} : this['KotlinJS TEST'], kotlin);
